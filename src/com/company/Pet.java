package com.company;

import java.util.Arrays;

class Pet {
    // field
    private String species, nickname;
    private String[] habits;
    private int age, trickLevel;

    // constructors
    public Pet() {
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    // methods
    public void eat() {
        System.out.println("I am eating.");
    }

    public void respond() {
        System.out.println("Hello, owner. I am - " + nickname + ". I miss you!");
    }

    public void foul() {
        System.out.println("I need to cover it up.");
    }

    // Override methods
    @Override
    public String toString() {
        return species + "{" +
                "nickname = '" + nickname + '\'' +
                ", age = " + age +
                ", trickLevel = " + trickLevel +
                ", habits = " + Arrays.toString(habits) + "}";
    }

    // setters
    public void set(String key, String a) {
        switch (key) {
            case "nickname":
                this.nickname = a;
                break;
            case "species":
                this.species = a;
                break;
        }
    }

    public void set(String key, int a) {
        switch (key) {
            case "age":
                this.age = a;
                break;
            case "trickLevel":
                this.trickLevel = a;
                break;
        }
    }

    public void set(String key, String[] habits) {
        this.habits = habits;
    }

    // getters
    public String getString(String a) {
        switch (a) {
            case "nickname":
                return nickname;
            case "species":
                return species;
        }
        return Arrays.toString(habits);
    }

    public int getInt(String a) {
        switch (a) {
            case "age":
                return age;
        }
        return trickLevel;
    }
}