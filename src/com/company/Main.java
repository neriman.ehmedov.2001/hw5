package com.company;

public class Main {
    public static void main(String[] args) {
        Human mother = new Human("Rahima", "Ahmadov", 2000);
        Human father = new Human("Neriman", "Ahmadov", 2001);

        Family familyAhamdov = new Family(mother, father);
        mother.set("family", familyAhamdov);
        father.set("family", familyAhamdov);

        Human esma = new Human("Esma", "Ahmadov", 2020, 160,
                new String[][]{{"Sunday ", "do home work"},
                        {"Monday ", "go to courses; watch a film"}, {"Tuesday ", "do workout"},
                        {"Wednesday ", "read e-mails"}, {"Thursday ", "do shopping"},
                        {"Friday ", "do household"}, {"Saturday ", "visit grandparents"}});

        familyAhamdov.addChild(esma);
        esma.set("family", familyAhamdov);

        Pet esmasPet = new Pet("dog", "Rex",
                5, 49, new String[]{"eat, drink, sleep"});
        familyAhamdov.set("pet", esmasPet);
        esma.greetPet();
        esma.describePet();
        esmasPet.respond();
        esmasPet.eat();
        esmasPet.foul();

        System.out.println(esmasPet.toString());

        System.out.println(esma.toString());

        Human child1 = new Human();
        Human child2 = new Human();
        Human child3 = new Human();
        familyAhamdov.addChild(child1);
        familyAhamdov.addChild(child2);
        familyAhamdov.addChild(child3);
        familyAhamdov.countFamily();

        System.out.println();

        familyAhamdov.deleteChild(2);
        familyAhamdov.countFamily();

        System.out.println();

        familyAhamdov.deleteChild(child3);
        familyAhamdov.countFamily();
    }
}