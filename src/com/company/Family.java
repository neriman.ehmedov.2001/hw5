package com.company;

import java.util.Arrays;
import java.util.Objects;

class Family {
    // field
    private Human mother, father;
    private Human[] children;
    private Pet pet;

    // constructors
    public Family() {
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[]{};
    }

    // methods
    public int countFamily() {
        int members = 2 + children.length;
        System.out.println("Family members are: " + members);
        return members;
    }

    public void addChild(Human child) {
        Human[] temp = new Human[children.length + 1];
        for (int i = 0; i < children.length; i++) {
            temp[i] = children[i];
        }
        temp[children.length] = child;
        this.children = temp;
        System.out.println("The child has been added.");
    }

    public boolean deleteChild(int index) {
        if (index > children.length) {
            System.out.println("You write wrong index");
            return false;
        }

        Human[] temp = new Human[this.children.length - 1];
        for (int i = 0, j = 0; j < children.length - 1; i++, j++) {
            if (j != index) {
                temp[i] = children[j];
            }
        }

        this.children = temp;

        System.out.println("The child has been deleted.");
        return true;
    }

    public boolean deleteChild(Human child) {
        Human[] temp = new Human[this.children.length - 1];
        for (int i = 0, index = 0; i < children.length - 1; i++, index++) {
            if (children[i].equals(child) != true)
                temp[index] = children[i];
        }

        this.children = temp;

        System.out.println("The child has been deleted.");
        return true;
    }

    // Override methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        Family family = (Family) o;
        return Objects.equals(get("mother"), family.get("mother")) &&
                Objects.equals(get("father"), family.get("father")) &&
                Objects.equals(getPet("pet"), family.getPet("pet")) &&
                Arrays.equals(children, family.children);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(get("mother"), get("father"), getPet("pet"));
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother = " + mother +
                ", father = " + father +
                ", pet = " + pet +
                ", children = " + Arrays.toString(children) +
                '}';
    }

    // setters
    public void set(String key, Human human) {
        switch (key) {
            case "mother":
                this.mother = human;
                break;
            case "father":
                this.father = human;
                break;
        }
    }

    public void set(String key, Human[] child) {
        this.children = child;
    }

    public void set(String key, Pet pet) {
        this.pet = pet;
    }

    // getters
    public Human get(String key) {
        switch (key) {
            case "mother":
                return mother;
        }
        return father;
    }

    public Pet getPet(String key) {
        return pet;
    }

    public String getChildren() {
        return Arrays.toString(children);
    }
}