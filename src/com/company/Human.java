package com.company;

import java.util.Arrays;

class Human {
    // field
    private String name, surname;
    private int year, iq;
    private String[][] schedule;
    private Family family;

    // constructors
    public Human() {
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    // methods
    public void greetPet() {
        System.out.println("Hello, " + family.getPet("pet").getString("nickname") + '.');
    }

    public void describePet() {
        System.out.print("I have a " + family.getPet("pet").getString("species") +
                ", he is " + family.getPet("pet").getInt("age") + " years old, he is ");
        System.out.println((family.getPet("pet")
                .getInt("trickLevel") > 50) ? "very sly." : "almost not sly.");
    }

    // Override methods
    @Override
    public String toString() {
        return "Human{" +
                "name = '" + name + '\'' +
                ", surname = '" + surname + '\'' +
                ", year = " + year +
                ", iq = " + iq +
                ", schedule = " + Arrays.deepToString(schedule) + "}";
    }

    // setters
    public void set(String key, String b) {
        switch (key) {
            case "name":
                this.name = b;
                break;
            case "surname":
                this.surname = b;
                break;
        }
    }

    public void set(String key, int b) {
        switch (key) {
            case "year":
                this.year = b;
                break;
            case "iq":
                this.iq = b;
                break;
        }
    }

    public void set(String key, Family b) {
        this.family = b;
    }

    public void set(String key, String[][] b) {
        this.schedule = b;
    }

    // getters
    public String getString(String a) {
        switch (a) {
            case "name":
                return name;
            case "surname":
                return surname;
        }
        return Arrays.toString(schedule);
    }

    public int getInt(String a) {
        switch (a) {
            case "iq":
                return iq;
        }
        return year;
    }

    public Family getFamily(String a) {
        return family;
    }
}